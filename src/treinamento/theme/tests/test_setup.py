# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from treinamento.theme.testing import TREINAMENTO_THEME_INTEGRATION_TESTING  # noqa
from plone import api

import unittest


class TestSetup(unittest.TestCase):
    """Test that treinamento.theme is properly installed."""

    layer = TREINAMENTO_THEME_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if treinamento.theme is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'treinamento.theme'))

    def test_browserlayer(self):
        """Test that ITreinamentoThemeLayer is registered."""
        from treinamento.theme.interfaces import (
            ITreinamentoThemeLayer)
        from plone.browserlayer import utils
        self.assertIn(ITreinamentoThemeLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = TREINAMENTO_THEME_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['treinamento.theme'])

    def test_product_uninstalled(self):
        """Test if treinamento.theme is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'treinamento.theme'))

    def test_browserlayer_removed(self):
        """Test that ITreinamentoThemeLayer is removed."""
        from treinamento.theme.interfaces import ITreinamentoThemeLayer
        from plone.browserlayer import utils
        self.assertNotIn(ITreinamentoThemeLayer, utils.registered_layers())
