# -*- coding: utf-8 -*-
from plone.app.robotframework.testing import REMOTE_LIBRARY_BUNDLE_FIXTURE
from plone.app.testing import applyProfile
from plone.app.testing import FunctionalTesting
from plone.app.testing import IntegrationTesting
from plone.app.testing import PLONE_FIXTURE
from plone.app.testing import PloneSandboxLayer
from plone.testing import z2

import treinamento.theme


class TreinamentoThemeLayer(PloneSandboxLayer):

    defaultBases = (PLONE_FIXTURE,)

    def setUpZope(self, app, configurationContext):
        # Load any other ZCML that is required for your tests.
        # The z3c.autoinclude feature is disabled in the Plone fixture base
        # layer.
        self.loadZCML(package=treinamento.theme)

    def setUpPloneSite(self, portal):
        applyProfile(portal, 'treinamento.theme:default')


TREINAMENTO_THEME_FIXTURE = TreinamentoThemeLayer()


TREINAMENTO_THEME_INTEGRATION_TESTING = IntegrationTesting(
    bases=(TREINAMENTO_THEME_FIXTURE,),
    name='TreinamentoThemeLayer:IntegrationTesting'
)


TREINAMENTO_THEME_FUNCTIONAL_TESTING = FunctionalTesting(
    bases=(TREINAMENTO_THEME_FIXTURE,),
    name='TreinamentoThemeLayer:FunctionalTesting'
)


TREINAMENTO_THEME_ACCEPTANCE_TESTING = FunctionalTesting(
    bases=(
        TREINAMENTO_THEME_FIXTURE,
        REMOTE_LIBRARY_BUNDLE_FIXTURE,
        z2.ZSERVER_FIXTURE
    ),
    name='TreinamentoThemeLayer:AcceptanceTesting'
)
